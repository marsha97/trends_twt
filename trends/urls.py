from django.contrib import admin
from django.urls import path, re_path, include

urlpatterns = [
    path('', include('utils.urls')),
]
