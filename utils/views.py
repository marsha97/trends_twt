from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponseNotAllowed, HttpResponseBadRequest

from utils.analyze.twitter_mod import TwitterCrawl

"""
The core of the program
"""
def SentimentView(request):
    text = request.GET.get('keyword', '')
    if text == '':
        return JsonResponse({'error': 'Bad request'}, status=HttpResponseBadRequest.status_code)
        
    twt_obj = TwitterCrawl(text, exact=True)
    return JsonResponse(twt_obj.process_query_senti())

def ProductSearchView(request):
    text = request.GET.get('keyword', '')
    if text == '':
        return JsonResponse({'error': 'Bad request'}, status=HttpResponseBadRequest.status_code)

    twt_obj = TwitterCrawl(text)
    return JsonResponse(twt_obj.extract_product(), safe=False)

def CommentTestView(request):
    text = request.GET.get('comment', '')
    if text == '':
        return JsonResponse({'error': 'Bad request'}, status=HttpResponseBadRequest.status_code)

    twt_obj = TwitterCrawl(text)
    return JsonResponse(twt_obj.process_test_comment())