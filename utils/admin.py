from django.contrib import admin
from django.contrib.admin import AdminSite
from django.conf.urls import re_path

from django.contrib.auth.models import Group, User
from django.contrib.auth.admin import GroupAdmin, UserAdmin

