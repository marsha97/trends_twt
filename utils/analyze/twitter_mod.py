import json
import urllib
import re
import os
import html
import configparser
import time

from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk import pos_tag
from nltk import FreqDist

from TwitterSearch import TwitterSearchOrder
from TwitterSearch import TwitterSearchException
from TwitterSearch import TwitterSearch

from . import sentiment_tfidf
from . import keyword_mod
from . import product_finder as p_finder

# TODO ZeroDivisionError
class TwitterCrawl:
    """
    Class for crawling twitter data
    """

    def __init__(self, keyword, exact=False):
        self.exact = exact
        self.keyword = keyword.lower()
        self.lang = 'en'
        self.ignored = ['-giveway', '-win',  '-click here','-discount', '-deal', '-deals', '-offer', '-RT']
        self.tso = object()
        self.ts = object()
        self.proc_keyword = ""

    def crawl_twitter(self):
        configp = configparser.ConfigParser()
        fullpath = os.path.realpath(__file__)
        fullpath = os.path.dirname(fullpath) + "/"

        configp.read_file(open(fullpath + 'config.ini'))
        twt_conf = configp['twitter.com']
        url = self.create_url()
        try:
            self.tso = TwitterSearchOrder()

            self.tso.set_search_url(url)

            self.ts = TwitterSearch(
                consumer_key=twt_conf['CONSUMER_KEY'],
                consumer_secret=twt_conf['CONSUMER_SECRET'],
                access_token=twt_conf['ACCESS_TOKEN'],
                access_token_secret=twt_conf['ACCESS_TOKEN_SECRET']
            )

            tweets = self.ts.search_tweets(self.tso)
            return tweets

        except TwitterSearchException as e:
            print(e)

    def process_query_senti(self):
        tweets = self.crawl_twitter()

        extracted = []
        raw_extract = []
        for index, tweet in enumerate(self.ts.search_tweets_iterable(self.tso)):
            if(self.ts.get_statistics()[0] > 5):
                break
            scores = {}
            fulltext = tweet['full_text']
            raw_extract.append({
               'raw_text': fulltext,
               'text': '',
               'label': '',
            })

        extracted = self.normalize_tweet(raw_extract)
        extracted = self.get_sentiment(extracted)
        extracted['queries_done'] = self.ts.get_statistics()[0] - 1

        return extracted

    def process_test_comment(self):
        comment = [
            {
                'raw_text': self.keyword,
                'text': '',
                'label': '',
            }
        ]

        extracted = self.normalize_tweet(comment, limit=False)
        extracted = self.get_sentiment(extracted)
        extracted['queries_done'] = 0

        return extracted

    def extract_product(self):
        tweets = self.crawl_twitter()
        
        result = {}
        products = []
        raw_tweets = []

        for tweet in self.ts.search_tweets_iterable(self.tso):
            if(self.ts.get_statistics()[0] > 5):
                break
            raw_tweets.append(tweet['full_text'])

        symbol_remover = "([^a-zA-Z0-9\'’, \t])"
        mention_remover = "(@[a-zA-Z0-9]+)"
        # hashtag_remover = "(#[a-zA-Z0-9]+)"
        link_remover = "(https?:\/\/\S+)"
        RT_remover = "(\\brt\\b)"
        removers = [link_remover, mention_remover, symbol_remover, RT_remover]
        regex = "|".join(removers)

        result['data'] = []
        result['raw_tweets'] = []
        result['products'] = []
        normals = []
        result['queries_done'] = self.ts.get_statistics()[0] - 1
        for i, r in enumerate(raw_tweets):
            data = {}
            parser = html.parser.HTMLParser()
            r = parser.unescape(r)
            r = " ".join(re.sub("(’)", "'", r).split())
            r = " ".join(re.sub("(,)", " , ", r).split())
            normalized = re.sub(regex, ' ', r).split()
            if self.is_spam(' '.join(normalized)):
                continue
            if normalized in normals:
                continue
            result['raw_tweets'].append(r)
            tokenized = word_tokenize(r)
            normals.append(normalized)
            product = p_finder.analyze(normalized, self.keyword)

            data['id'] = i
            data['raw_tweet'] = r
            data['clean'] = " ".join(normalized)
            data['token'] = normalized
            
            result['data'].append(data)
            if len(product) > 0:
                products.extend(product)
        counted = FreqDist(products)
        result['products'] = [{'name': p[0], 'count': p[1]} for p in counted.most_common(len(counted))]
        result['tweets_count'] = len(result['raw_tweets'])
        result['keywrod'] = self.keyword
        result['stop_keyword'] = self.proc_keyword
        return result


    def get_sentiment(self, tweet_data):
        pos = neg = 0
        sentiment_tweet= {
            "data":[]
        }
        for index, data in enumerate(tweet_data):
            label = sentiment_tfidf.sentiment(data['text'])[0]
            data['label'] = ""
            if (label == 1):
                data['label'] = "pos"
                pos = pos + 1
            else:
                data['label'] = "neg"
                neg = neg + 1
            sentiment_tweet['data'].append(data)
        try:
            sentiment_tweet['positive'] = round(pos / len(sentiment_tweet['data']),2)
            sentiment_tweet['negative'] = round(neg / len(sentiment_tweet['data']),2)
        except ZeroDivisionError:
            sentiment_tweet['positive'] = 0
            sentiment_tweet['negative'] = 0
        sentiment_tweet['tweets_count'] = len(sentiment_tweet['data'])
        sentiment_tweet['keyword'] = self.keyword
        return sentiment_tweet

    def normalize_tweet(self, tweet_data, limit=True):
        stemmer = PorterStemmer()

        symbol_remover = "([^a-zA-Z0-9])"
        mention_remover = "(@[a-zA-Z0-9_-]+)"
        hashtag_remover = "(#[a-zA-Z0-9]+)"
        link_remover = "(https?:\/\/\S+)"
        RT_remover = "(\\brt\\b)"
        www_remover = '(www.[^ ]+)'
        negations_dic = {"isn't":"is not", "aren't":"are not", "wasn't":"was not", "weren't":"were not",
                "haven't":"have not","hasn't":"has not","hadn't":"had not","won't":"will not",
                "wouldn't":"would not", "don't":"do not", "doesn't":"does not","didn't":"did not",
                "can't":"can not","couldn't":"could not","shouldn't":"should not","mightn't":"might not",
                "mustn't":"must not"}
        neg_pattern = re.compile(r'\b(' + '|'.join(negations_dic.keys()) + r')\b')
        removers = [mention_remover, hashtag_remover, link_remover,www_remover, symbol_remover, RT_remover]
        regex = "|".join(removers)

        normalized = []
        for index, data in enumerate(tweet_data):
            data['text'] = []
            # normalize html parsing
            parser = html.parser.HTMLParser()

            data['raw_text'] = " ".join(re.sub("(’)", "'", data['raw_text']).split())
            data['raw_text'] = parser.unescape(data['raw_text'])

            # removed_sym = re.sub(regex, ' ', data['raw_text'].lower()).split()
            removed_sym = " ".join(re.sub(regex, ' ', data['raw_text'].lower()).split())
            
            if self.is_spam(removed_sym):
                continue
            removed_sym = neg_pattern.sub(lambda x: negations_dic[x.group()], removed_sym)

            # tokenize
            tokenized = word_tokenize(removed_sym)

            stop_words = set(stopwords.words('english'))

            filtered = [w for w in tokenized if w not in stop_words]

            stemmed = [stemmer.stem(w) for w in filtered]

            data['id'] = index
            data['token'] = tokenized
            data['clean'] = removed_sym
            data['text'] = filtered
            data['stem'] = stemmed
            if len([t['text'] for t in normalized if t['text'] == data['text']]) > 0:
                continue
            if limit:
                if len(data['text']) > 3:
                    normalized.append(data)
            else:
                normalized.append(data)
        return normalized
        
    def is_spam(self, tweet):
        trash_words = ['giveaway', 
        'i liked a video', 
        'i added a video to a playlist', 
        'i just uploaded a new video to' , 
        'recover your messages',
        'lost messages',
        'recovering messages files',
        'unlock android phone if you forget',
        'device lock',
        'how to'
        ]
        return any([word in tweet.lower() for word in trash_words])

    def create_url(self):
        if self.exact is not True:
            keyword_obj = keyword_mod.KeywordPreprocessing(self.keyword)        
            filtered_k = keyword_obj.process_keyword()
            self.proc_keyword = " ".join(filtered_k)
        else:
            self.keyword = "\"" + self.keyword + "\""
            filtered_k = self.keyword.split()
        filtered_k = filtered_k + self.ignored

        query = urllib.parse.quote(' '.join(filtered_k))
        count = str(100)
        result_type = 'mixed'
        tweet_mode = 'extended'
        url = '?q='+query+'&lang='+self.lang+'&count='+count + \
            '&result_type='+result_type+'&tweet_mode='+tweet_mode
        return url
