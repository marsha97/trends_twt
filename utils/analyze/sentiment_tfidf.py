import pickle
import numpy as np
import os
from nltk.stem.porter import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
from . import sentiment_tfidf_save


analyzer = TfidfVectorizer().build_analyzer()
stemmer = PorterStemmer()

fullpath = os.path.realpath(__file__)
fullpath = os.path.dirname(fullpath) + "/"

# with open(fullpath + "pickled_algos/stemmed_words.pickle", "rb") as stem_f:
#     print(pickle.load(stem_f))

with open(fullpath + "pickled_algos/tf_idf_unigram_stemmed.pickle", "rb") as classifier_f:
    clf = pickle.load(classifier_f)

with open(fullpath + "pickled_algos/tf_idf_unigram_vectorizer_stemmed.pickle", "rb") as vector_f:
    vectorizer = pickle.load(vector_f)

def sentiment(text):
    text_arr = np.array([" ".join(text)])
    text_vector = vectorizer.transform(text_arr)
    return(clf.predict(text_vector))
