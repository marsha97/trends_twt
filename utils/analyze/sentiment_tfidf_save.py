import pandas as pd
import os
import json
import pickle
import numpy as np
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn import naive_bayes
from sklearn.metrics import roc_auc_score
from sklearn.utils import shuffle
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

fullpath = os.path.realpath(__file__)
fullpath = os.path.dirname(fullpath) + "/"
analyzer = TfidfVectorizer().build_analyzer()
stemmer = PorterStemmer()
def stemmed_words(doc):
    al = analyzer(doc)
    return (stemmer.stem(w) for w in al)
    
def train_sample():
   
    # fullpath = os.path.realpath(__file__)
    # fullpath = os.path.dirname(fullpath) + "/"

    my_df = pd.read_csv(fullpath + "sentiment_data/clean_tweet_suffled.csv", encoding='utf-8')
   
    my_df_test = pd.read_csv(fullpath + "sentiment_data/clean_tweet_test_shuffled.csv", encoding='utf-8')
   
    my_df = my_df[:len(my_df) - round(len(my_df)*1/100)]
    my_df = my_df[:len(my_df) - round(len(my_df)*1/100)]
    stopset = set(stopwords.words('english'))
    vectorizer = TfidfVectorizer(use_idf=True,lowercase=True, strip_accents='ascii', stop_words=stopset, ngram_range=(1,1), analyzer=stemmed_words)
    y_train = my_df.target
    X_train = vectorizer.fit_transform(my_df.text)
    with open(fullpath + "pickled_algos/stemmed_words.pickle", "wb") as stem_f:
        pickle.dump(stemmed_words, stem_f)
    with open(fullpath + "pickled_algos/tf_idf_unigram_vectorizer_stemmed.pickle", "wb") as vec_f:
        pickle.dump(vectorizer, vec_f)
    y_test = my_df_test.target
    X_test = vectorizer.transform(my_df_test.text)
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.01)
    # print(X_test.shape)
    clf = naive_bayes.MultinomialNB()
    clf.fit(X_train, y_train)
    with open(fullpath + "pickled_algos/tf_idf_unigram_stemmed.pickle", "wb") as clf_f:
        pickle.dump(clf, clf_f)
    print(roc_auc_score(y_test, clf.predict_proba(X_test)[:,1]))

if __name__ == "__main__":
    my_df = pd.read_csv(fullpath + "sentiment_data/clean_tweet_suffled.csv", encoding='utf-8')
    my_df = shuffle(my_df)
    my_df.to_csv(fullpath + "sentiment_data/clean_tweet_suffled.csv", encoding='utf-8', index=False)
    my_df = pd.read_csv(fullpath + "sentiment_data/clean_tweet_suffled.csv", encoding='utf-8')
    my_df[(len(my_df)-round(len(my_df)*1/100)):].to_csv(fullpath + "sentiment_data/clean_tweet_test_shuffled.csv", encoding='utf-8', index=False)
    train_sample()