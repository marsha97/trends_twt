import os
import re
import json

from nltk import pos_tag

fullpath = os.path.realpath(__file__)
fullpath = os.path.dirname(fullpath) + "/"
with open(fullpath + 'crf_data/brand_dict.json') as brand_f:
    brands = json.load(brand_f)

with open(fullpath + 'crf_data/brand_dict_v3.json') as brand_f_2:
    brands2 = json.load(brand_f_2)

with open(fullpath + 'crf_data/grammar_dict.json') as grammar_f:
    grammars = json.load(grammar_f)

with open(fullpath + 'crf_data/en_common_dict.json') as common_f:
    commons = json.load(common_f)


def tag_word(words):
    return pos_tag(words)


def analyze(token, keyword):
    # tagged = pos_tag(token)
    passed_index = []
    products = []
    word = ' '.join(token)
    tok = word.lower().split()
    
    for brand in brands2:
        is_brand = False
        pr = []        
        for b in brand:
            if b in tok and b not in grammars and b not in commons:
                is_brand = True
                pr.append(b)
            else:
                is_brand = False
                break
        if len(pr) > 0 and is_brand:
            products.append(pr)
    brand_range = []
   
    for product in products:
        ranges = []
        for p in product:
            ranges.append(tok.index(p))
        brand_range.append([product, ranges[0], ranges[-1]])        
    
    products = []
    for br, start, end in brand_range:
        br = ' '.join(br)
        product_tmp = token[end + 1: len(token)]
        tagged_prod_tmp = pos_tag(product_tmp)

        for tagged in tagged_prod_tmp:
            w = tagged[0]
            label = tagged[1]
            if re.search("[^a-zA-Z0-9]", w) is not None:
                break
            elif w.lower() not in commons and w not in grammars:
                br = br + " " + w.lower()
            elif label == "NNP" and re.search("[^a-zA-Z0-9]", w) is None and len(br.split()) < 3:
                br = br + " " + w.lower()
            else:
                break
        products.append(br)

    split_prd = [splited for product in products for splited in product.split()]        

    return products

def next_is_brand(word):
    tagged = pos_tag(word)
    if tagged[1] == 'PRP$':
        return True