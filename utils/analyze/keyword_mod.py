from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

class KeywordPreprocessing:
    """
    Class for keyword processing
    """
    def __init__(self, keyword):
        self.keyword = keyword.lower().strip()

    def process_keyword(self):
        """
        Process english keyword
        """
        tokenized_words = word_tokenize(self.keyword)

        # Stopwords removal
        filtered_words = self.remove_stopword(tokenized_words)

        return filtered_words
        
    def remove_stopword(self, tokenized_words):
        stop_words = set(stopwords.words('english'))
        filtered_words = []
        for word in tokenized_words:
            if word not in stop_words:
                filtered_words.append(word)
        return filtered_words                