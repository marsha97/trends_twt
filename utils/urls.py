from django.conf.urls import re_path
from utils import views

app_name = 'utils'
urlpatterns = [
    re_path(r'^tweets/sentiment/$', views.SentimentView, name="twt_sentiment"),
    re_path(r'^tweets/products/$', views.ProductSearchView, name="twt_products"),
    re_path(r'^tweets/comment/$', views.CommentTestView, name="test_comment"),
]