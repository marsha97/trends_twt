## Caution
Rename CONFIG.INI_EXAMPLE in trends/utils/analyze/CONFIG.INI_EXAMPLE into config.ini and change the parameter according to your twitter API.

## Backend Program for Trends
This program is a backend part of Trends. Made with Django and using Twitter API.
With help of TwitterSearch Library by ckoepp: https://github.com/ckoepp/TwitterSearch

## What is Trends?
Trends is a web that can help you find popular products in Twitter by comparing your keyword to the found tweets, and you can also check the sentiment value of certain products. This app provides the REST API used by the web.

## Limitation
The Twitter search is limited up to 5 query (about 500 tweets) to save the Twitter API limitation. And the compare product feature is limited to 3 products since the Twitter API limimation.