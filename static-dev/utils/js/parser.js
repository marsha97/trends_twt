$(function(){
    $('body').on('click', '.generator', getBrandJson)
})

function getBrandJson(){
    $.ajax({url: '/brand/parser/', success: processBrand})
}

function processBrand(result){
    generateBrandTable(result)
    resetBrand(result)
}

function generateBrandTable(result){
    let table = $("<table></table>").addClass("table table-striped")
    $('.generator-result').append(table)
    let thead = $("<thead></thead>")
    let tbody = $("<tbody></tbody>")
    $("table").append(thead, tbody)
    let tr = $("<tr></tr>")
    $("thead").append(tr)

    let th_no = $("<th></th>").text("No.")
    let th_brand = $("<th></th>").text("Item")
    let th_categories = $("<th></th>").text("Category")
    $("thead tr").append(th_no, th_brand, th_categories)

    result.forEach(function(item, index){
        $("tbody").append("<tr></tr>")
        let td_no = $("<td></td>").text(index + 1)
        let td_brand = $("<td></td>").text(item['brand_name'])
        let td_category = $.parseJSON(item['brand_categories']).join(", ")
        $("tbody tr:last").append(td_no, td_brand, td_category)
    })
}

function resetBrand(result_to_send){
    let api_url =  '/api/v1/brands/'
    $.ajax({
        url: api_url,
        type: 'DELETE',
        beforeSend: authCSRF,
        success: function(result){
           $.ajax({
               url: api_url,
               type: 'POST',
               contentType: "application/json; charset=utf-8",
               dataType: "json", 
               data: JSON.stringify(result_to_send),
               beforeSend: authCSRF,
               complete: function(xhr, status){
                   alert(status)
               },
               error: function(xhr, status,error){
                   alert(error)
                   console.log(JSON.stringify(result_to_send))
               },
           })
        },
        error: function(xhr, status, error){
            alert(error)
        },
    })
}

function authCSRF(xhr, settings){
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
        // Only send the token to relative URLs i.e. locally.
        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
}